﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rudimentary_Calculator;

namespace OperatorTests
{
    [TestClass]
    public class ExponentiationOperatorTests
    {
        [TestMethod]
        public void ThreeByThePowerOfTwo()
        {
            Expression expression = new Expression("3^2");
            ExpressionSolver solver = new ExpressionSolver(expression);
            Assert.AreEqual(9.0, solver.Execute());
        }
    }
}
