﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Rudimentary_Calculator;

namespace OperatorTests
{
    [TestClass]
    public class AdditionOperatorTests
    {
        [TestMethod]
        public void TwoPlusTwo()
        {
            Expression expression = new Expression("2+2");
            ExpressionSolver solver = new ExpressionSolver(expression);
            Assert.AreEqual(4.0, solver.Execute());
        }

        [TestMethod]
        public void OnePlusTwoPlusThree()
        {
            Expression expression = new Expression("1+2+3");
            ExpressionSolver solver = new ExpressionSolver(expression);
            Assert.AreEqual(6.0, solver.Execute());
        }
    }
}
