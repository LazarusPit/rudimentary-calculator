﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rudimentary_Calculator;

namespace OperatorTests
{
    [TestClass]
    public class SubtractionOperatorTests
    {
        [TestMethod]
        public void TenMinusThree()
        {
            Expression expression = new Expression("10-3");
            ExpressionSolver solver = new ExpressionSolver(expression);
            Assert.AreEqual(7.0, solver.Execute());
        }

        [TestMethod]
        public void FiveMinusTen()
        {
            Expression expression = new Expression("5-10");
            ExpressionSolver solver = new ExpressionSolver(expression);
            Assert.AreEqual(-5.0, solver.Execute());
        }
    }
}
