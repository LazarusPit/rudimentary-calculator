﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rudimentary_Calculator
{
    public class CharType
    {
        public enum Type
        {
            Operand, Operator
        }

        public static Type Parse(char c)
        {
            return char.IsDigit(c) || c == '.' ? Type.Operand : Type.Operator;
        }
    }
}
