﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rudimentary_Calculator
{
    public class ExpressionSegment
    {
        public readonly CharType.Type segmentType;
        public readonly string value;

        public ExpressionSegment(string value, CharType.Type segmentType)
        {
            this.value = value;
            this.segmentType = segmentType;
        }

        public double AsNumber()
        {
            if (segmentType == CharType.Type.Operator)
            {
                throw new Exception("Unable to convert an operator to a numeric value.");
            }
            return double.Parse(value);
        }

        public override string ToString()
        {
            return value;
        }
    }
}
