﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rudimentary_Calculator
{
    public class ExpressionSolver
    {
        private readonly Expression expression;

        public ExpressionSolver(Expression expression)
        {
            this.expression = expression;
        }

        public double Execute()
        {
            Operator[] operatorPriority = new Operator[] { new ExponentiationOperator(), new DivisionOperator(), new MultiplicationOperator(), new AdditionOperator(), new SubtractionOperator() };
            expression.Validate();
            List<ExpressionSegment> segments = expression.segments;
            foreach (Operator operatorToFind in operatorPriority)
            {
                for (int segmentIndex = 1; segmentIndex < segments.Count - 1; segmentIndex++)
                {
                    if (segments[segmentIndex].segmentType != CharType.Type.Operator) { continue; }
                    Operator operatorFound = Operator.Parse(segments[segmentIndex].value);
                    if (operatorFound == null) { continue; }
                    if (operatorToFind.GetType() == operatorFound.GetType())
                    {
                        double leftOperand = segments[segmentIndex - 1].AsNumber();
                        double rightOperand = segments[segmentIndex + 1].AsNumber();
                        string operandResult = operatorFound.Use(leftOperand, rightOperand).ToString();
                        segments.RemoveRange(segmentIndex - 1, 3);
                        segments.Insert(segmentIndex - 1, new ExpressionSegment(operandResult, CharType.Type.Operand));
                        segmentIndex--;
                    }
                }
            }
            return segments.Count() > 1
                ? throw new Exception("Unable to parse complete expression. Leftover: " + string.Join(" ", segments))
                : (segments.Count == 1) ? segments[0].AsNumber() : 0.0;
        }
    }
}
