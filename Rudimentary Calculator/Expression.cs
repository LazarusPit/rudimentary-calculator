﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rudimentary_Calculator
{
    public class Expression
    {
        public readonly string text;
        public readonly List<ExpressionSegment> segments;

        public Expression(string text)
        {
            this.text = text;
            segments = new List<ExpressionSegment>();
            CharType.Type? currentCharType = null;
            StringBuilder segmentValue = new StringBuilder();
            foreach (char c in this.text)
            {
                if (c == ' ') { continue; }
                CharType.Type parsedType = CharType.Parse(c);
                if (parsedType != currentCharType)
                {
                    if (currentCharType != null)
                    {
                        segments.Add(new ExpressionSegment(segmentValue.ToString(), currentCharType.Value));
                        _ = segmentValue.Clear();
                    }
                    currentCharType = parsedType;
                }
                _ = segmentValue.Append(c);
            }
            if (currentCharType != null)
            {
                segments.Add(new ExpressionSegment(segmentValue.ToString(), currentCharType.Value));
            }
        }

        public void Validate()
        {
            if (segments.Count < 1)
            {
                throw new Exception("Empty input");
            }
            CharType.Type? previousType = null;
            foreach (ExpressionSegment segment in segments)
            {
                // first segment 
                if (previousType == null)
                {
                    // an operator (except perhaps minus or plus)
                    if (segment.segmentType == CharType.Type.Operator && Operator.Parse(segment.value).CanStartWith) {
                        throw new Exception("Starting with a slash operator is not possible.");
                    }
                }
                if (segment.segmentType == CharType.Type.Operator)
                {
                    if (previousType != CharType.Type.Operand)
                    {
                        throw new Exception("Two consecutive operators detected. This is not permitted.");
                    }
                }
                previousType = segment.segmentType;
            }
            if (previousType != null)
            {
                if (previousType != CharType.Type.Operand)
                {
                    throw new Exception("The last segment should be a value and not an operator.");
                }
            }
        }
    }
}
