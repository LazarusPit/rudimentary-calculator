﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rudimentary_Calculator
{

    public abstract class Operator
    {
        public abstract double Use(double leftOperand, double rightOperand);

        public static Operator Parse(string value)
        {
            switch (value)
            {
                case "+": return new AdditionOperator();
                case "-": return new SubtractionOperator();
                case "*": return new MultiplicationOperator();
                case "/": return new DivisionOperator();
                case "^": return new ExponentiationOperator();
                default: return null;
            }
        }

        public virtual bool CanStartWith => false;
        public virtual bool CanEndWith => false;
    }

    internal class AdditionOperator : Operator
    {
        public override double Use(double leftOperand, double rightOperand)
        {
            return leftOperand + rightOperand;
        }

        public override bool CanStartWith => true;
    }

    internal class SubtractionOperator : Operator
    {
        public override double Use(double leftOperand, double rightOperand)
        {
            return leftOperand - rightOperand;
        }

        public override bool CanStartWith => true;
    }

    internal class DivisionOperator : Operator
    {
        public override double Use(double leftOperand, double rightOperand)
        {
            return leftOperand / rightOperand;
        }
    }

    internal class MultiplicationOperator : Operator
    {
        public override double Use(double leftOperand, double rightOperand)
        {
            return leftOperand * rightOperand;
        }
    }

    internal class ExponentiationOperator : Operator
    {
        public override double Use(double leftOperand, double rightOperand)
        {
            return Math.Pow(leftOperand, rightOperand);
        }
    }
}
