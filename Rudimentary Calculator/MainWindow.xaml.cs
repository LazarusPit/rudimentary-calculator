﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Rudimentary_Calculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        

        public MainWindow()
        {
            InitializeComponent();
        }

        private string OutputValue
        {
            get => (lblOutput.Content == null) ? "" : lblOutput.Content.ToString();
            set => lblOutput.Content = value;
        }

        private void Digit_Click(object sender, RoutedEventArgs e)
        {
            if (!(sender is Button)) { return; }
            Button button = sender as Button;
            string caption = button.Content.ToString();
            OutputValue = (OutputValue == "0") ? caption : OutputValue + caption;
        }

        private void Operator_Click(object sender, RoutedEventArgs e)
        {
            if (!(sender is Button)) { return; }
            string state = OutputValue;
            char operatorValue = (sender as Button).Content.ToString()[0];
            // EndsWithAny operator
            if (CharType.Parse(state.Last()) == CharType.Type.Operator)
            {
                // different operator
                if (state.Last() != operatorValue)
                {
                    state = state.Remove(state.Length - 1, 1);
                    state += operatorValue;
                }
                else { return; }
            }
            else if (state.EndsWith("."))
            {
                state += "0";
            }
            else { state += operatorValue; }
            OutputValue = state;
        }

        private void Negate_Click(object sender, RoutedEventArgs e)
        {
            string state = OutputValue;
            int charIndex = state.Length - 1;
            bool handled = false;
            while (!handled && charIndex >= 0)
            {
                if (state[charIndex] == '+' || state[charIndex] == '-')
                {
                    bool wasPlus = state[charIndex] == '+';
                    state = state.Remove(charIndex, 1);
                    handled = true;
                    if (wasPlus)
                    {
                        state = state.Insert(charIndex, "-");
                    }
                    else if (charIndex > 0)
                    {
                        state = state.Insert(charIndex, "+");
                    }
                }
                else { charIndex--; }
            }
            if (!handled)
            {
                state = '-' + state;
            }
            OutputValue = state;
        }

        private void Dot_Click(object sender, RoutedEventArgs e)
        {
            string state = OutputValue;
            int dotIndex = state.LastIndexOf(".");
            int operatorIndex = state.Select((c, index) => (CharType.Parse(c) == CharType.Type.Operator) ? -1 : index).Last();
            if (operatorIndex == state.Length - 1 || state.Length == 0)
            {
                state += "0";
            }
            if (dotIndex > -1 && dotIndex > operatorIndex)
            {
                state = state.Remove(dotIndex, 1);
            }
            OutputValue = state + ".";
        }

        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            int outputLength = OutputValue.Length;
            if (outputLength > 0) {
                int equalsIndex = OutputValue.IndexOf('=');
                if (equalsIndex > -1)
                {
                    OutputValue = OutputValue.Remove(equalsIndex);
                }
                else
                {
                    OutputValue = OutputValue.Remove(outputLength - 1, 1);
                }
            }
        }

        private void Solve_Click(object sender, RoutedEventArgs e)
        {
            string state = OutputValue.Trim();
            if (state == "") { return; }
            if (state.Contains('=')) { return; }
            Expression expression = new Expression(state);
            ExpressionSolver solver = new ExpressionSolver(expression);
            double solution = solver.Execute();
            OutputValue += '=' + solution.ToString();
        }
    }
}
